package com.bit.stefanus.training.microservices.frontend.service;

/**
 *
 * @author Stefanus
 */
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("default")
@FeignClient(value = "pembayaranLocal", fallback = PembayaranClientServiceLocal.PembayaranClientServiceLocalFallback.class)
public interface PembayaranClientServiceLocal extends PembayaranClientService {

    @Component
    class PembayaranClientServiceLocalFallback
            extends PembayaranClientServiceFallback
            implements PembayaranClientServiceLocal {
    }
}
