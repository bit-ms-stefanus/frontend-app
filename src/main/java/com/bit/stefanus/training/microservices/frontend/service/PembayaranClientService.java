package com.bit.stefanus.training.microservices.frontend.service;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.frontend.dto.Customer;

import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

public interface PembayaranClientService {

    @GetMapping("/api/customer/")
    Iterable<Customer> daftarCustomer();
    //Iterable<Customer> daftarCustomer(@RequestHeader("Authorization") String authHeader);

    @GetMapping("/api/backendinfo")
    Map<String, Object> backendInfo();
}
