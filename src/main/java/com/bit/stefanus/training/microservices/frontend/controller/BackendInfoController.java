package com.bit.stefanus.training.microservices.frontend.controller;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.frontend.service.PembayaranClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BackendInfoController {

    @Autowired
    private PembayaranClientService pembayaranClientService;

    @GetMapping("/backendinfo")
    public ModelMap tampilkanBackendInfo() {
        return new ModelMap()
                .addAttribute("info",
                        pembayaranClientService.backendInfo());
    }
}
