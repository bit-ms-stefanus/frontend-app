package com.bit.stefanus.training.microservices.frontend.controller;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.frontend.service.PembayaranClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class CustomerController {

    @Autowired
    private PembayaranClientService pembayaranClientService;

    @GetMapping("/customer/list")
    public ModelMap dataCustomer(@RegisteredOAuth2AuthorizedClient("keycloak-client") 
            OAuth2AuthorizedClient authorizedClient) {
        String authHeader = "Bearer "
                + authorizedClient.getAccessToken().getTokenValue();

        log.info("Auth Header : {}", authHeader);

        return new ModelMap().addAttribute("dataCustomer",
                //pembayaranClientService.daftarCustomer(authHeader));
                pembayaranClientService.daftarCustomer());
    }
}
