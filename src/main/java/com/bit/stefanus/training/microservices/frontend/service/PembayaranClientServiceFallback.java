package com.bit.stefanus.training.microservices.frontend.service;

/**
 *
 * @author Stefanus
 */
import com.bit.stefanus.training.microservices.frontend.dto.Customer;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class PembayaranClientServiceFallback implements PembayaranClientService {

    @Override
    public Iterable<Customer> daftarCustomer() {
        log.info("Backend sedang offline, return list customer yang kosong");
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> backendInfo() {
        Map<String, Object> hasil = new LinkedHashMap<>();
        hasil.put("ipAddress", "-1.-1.-1.-1");
        hasil.put("port", -1);
        hasil.put("hostname", "undefined");
        return hasil;
    }
}
