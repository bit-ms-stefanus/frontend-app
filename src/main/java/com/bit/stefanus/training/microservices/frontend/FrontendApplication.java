package com.bit.stefanus.training.microservices.frontend;

import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;

@Slf4j
@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
public class FrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontendApplication.class, args);
    }

    @Bean
    public RequestInterceptor requestInterceptor(OAuth2AuthorizedClientService authorizedClientService) {
        return requestTemplate -> {
            log.info("Menjalankan interceptor feign untuk memasang access token");
            Authentication authentication
                    = SecurityContextHolder
                            .getContext()
                            .getAuthentication();
            log.info("Authentication : {}", authentication);

            OAuth2AuthorizedClient authorizedClient
                    = authorizedClientService.loadAuthorizedClient(
                            "keycloak-client", authentication.getName());
            String accessToken = authorizedClient.getAccessToken().getTokenValue();
            log.info("Access Token : {}", accessToken);

            requestTemplate.header(HttpHeaders.AUTHORIZATION,
                    "Bearer " + accessToken);
        };
    }
}
