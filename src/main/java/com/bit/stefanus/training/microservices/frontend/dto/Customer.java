package com.bit.stefanus.training.microservices.frontend.dto;

/**
 *
 * @author Stefanus
 */
import lombok.Data;

@Data
public class Customer {
    private String id;
    private String email;
    private String nama;
    private String noHp;
    private String alamat;
    private String foto;
}