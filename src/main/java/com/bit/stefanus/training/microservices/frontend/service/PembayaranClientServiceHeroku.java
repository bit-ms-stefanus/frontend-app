package com.bit.stefanus.training.microservices.frontend.service;

/**
 *
 * @author Stefanus
 */
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("heroku")
@FeignClient(value = "pembayaran", fallback = PembayaranClientServiceHeroku.PembayaranClientServiceHerokuFallback.class)
public interface PembayaranClientServiceHeroku extends PembayaranClientService {

    @Component
    class PembayaranClientServiceHerokuFallback
            extends PembayaranClientServiceFallback
            implements PembayaranClientServiceLocal {
    }
}
